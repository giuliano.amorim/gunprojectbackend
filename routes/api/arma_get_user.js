const express = require('express')
const auth = require('../../middleaware/auth')
const Product = require('../../models/product')
const router = express.Router()
const MSGS = require('../../messages')
const User = require('../../models/user')
const product = require('../../models/product')


// @route    POST /user/:armaId
// @desc     Get User by armaId
// @access   Private
router.post('/:userId-:productId', [], async (req, res, next) => {
  try {
    const id = req.params.productId
    const user_Id = req.params.userId
    let users = await User.findOne({ _id: user_Id })
    let user_arma = {
      iduser: users._id,
      user: users.name
    }
    const user = await Product.findOneAndUpdate({ _id: id }, { $push: { users: user_arma } }, { new: true })
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": MSGS.GENERIC_ERROR })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    GET /user/:userId
// @desc     DETAIL arma_get_user
// @access   Private
router.get('/:productId', async (req, res, next) => {
  try {
    const id = req.params.productId
    const produto = await Product.findOne({ _id: id })
    if (produto) {
      res.json(produto.users)
    } else {
      res.status(404).send({ "error": MSGS.USER404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


module.exports = router
