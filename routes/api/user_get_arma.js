const express = require('express')
const auth = require('../../middleaware/auth')
const Product = require('../../models/product')
const router = express.Router()
const MSGS = require('../../messages')
const User = require('../../models/user')
const config = require('config')


// @route    POST /user/:armaId
// @desc     Get Arma by UserId
// @access   Private
router.post('/:userId-:productId', [], async (req, res, next) => {
  try {
    const id = req.params.productId
    console.log(id)
    const user_Id = req.params.userId
    let products = await Product.findOne({ _id: id })

    let nova_arma = {
      idproduct: products._id,
      arma: products.title,
      photo: products.photo
    }

    //   if (!req.body.order){
    //     req.body.order = get_max_order(content, 'banner')
    //   }
    user = await User.findOneAndUpdate({ _id: user_Id }, { $push: { products: nova_arma } }, { new: true })
    if (user) {
      // content = get_complete_link(content)
      res.json(user)
    } else {
      res.status(404).send({ "error": MSGS.GENERIC_ERROR })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


// @route    GET /user/:userId
// @desc     DETAIL user_get_arma
// @access   Private
router.get('/:userId', auth, async (req, res, next) => {
  try {
    const id = req.params.userId
    const user = await User.findOne({ _id: id })
    let products = user.products
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH || config.get('BUCKET_PUBLIC_PATH')
    
    products = products.map(function(product) {
      product.photo = `${BUCKET_PUBLIC_PATH}${product.photo}`
      return product
    })  

    if (products) {
      res.json(products)
    } else {
      res.status(404).send({ "error": MSGS.USER404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})




module.exports = router

