const mongoose = require('mongoose');
const category = require('./category');
const user = require('./user');

const ProductSchema = new mongoose.Schema({
    photo: {
        type : String,
        required : true
    },
    title: {
        type : String,
        required : true
    },
    category:  [{

        categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'category'
     
        },

        name: { 
        type: String,
        ref: 'category'
    
        }
    }],

    users:  [{

        iduser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
       
        },

        user: { 
        type: String
     
        }
    }],

    description: {
        type : String,
        required : true
    },
    complete_description: {
        type : String
    },
    price: {
        type : Number,
        min: 1,
        required : true
    },
    last_modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    last_modification_date: {
        type: Date,
        default: Date.now
    }
}, { autoCreate : true })

module.exports = mongoose.model('product', ProductSchema);
