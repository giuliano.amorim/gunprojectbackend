const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({



    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    },

    products: [{

        idproduct: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'product'
        },

        arma: {
            type: String
        },

        photo: {
            type: String
        }
    }],

    password: {
        type: String,
        required: true,
        select: false
    },
    date: {
        type: Date,
        default: Date.now
    },

    is_active: {
        type: Boolean,
        default: true
    },

    is_admin: {
        type: Boolean,
        default: false
    },

}, { autoCreate: true })

module.exports = mongoose.model('user', UserSchema);
